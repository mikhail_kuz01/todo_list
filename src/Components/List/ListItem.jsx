import React, { useState } from 'react'
import axios from 'axios'

import Circle from '../Circle/Circle.jsx'
import Modal from '../Modal/Modal.jsx'

import RemoveSvg from '../../assets/img/remove.svg'

const ListItem = ({ item, icon, title, active, color, isRmv, onRmv, onClickItem, activeItem }) => {
    const [modalActive, setModalActive] = useState(false)
    const [elem, setElem] = useState(null)

    const CName = (activeItem && activeItem.id === item.id) || active ? 'todo__list__item active' : 'todo__list__item'

    const removeListItem = (item, confirm, e) => {
        e.preventDefault()
        if (confirm) {
            axios.delete('http://localhost:3001/lists/' + item.id).then(() => {
                onRmv(item.id)
            })
        }
        setModalActive(false)
    }

    const getCoords = () => {
        setTimeout(() => {
            let box = document.querySelector('#modal-window')
            box.style.marginTop = `0px`
            box.style.transition = 'all 0.3s'
        }, 1)
    }

    return (
        <li className={CName} onClick={onClickItem ? () => onClickItem(item) : null}>
            {icon ? (
                <img src={icon} alt="List icon" />
            ) : (
                <Circle color={color} />
            )}
            <span className='todo__list__item-text'>
                {title}
            </span>
            {isRmv && (
                <img 
                    src={RemoveSvg} 
                    alt="Remove icon" 
                    className='remove-btn' 
                    // onClick={() => removeListItem(item)}
                    onClick={(e) => {
                        setModalActive(true)
                        setElem(e)
                    }}
                />
            )}
            {modalActive && (
                getCoords(),
                <Modal 
                    isDelete
                    list
                    removeListItem={(confirm, e) => removeListItem(item, confirm, e)}
                />
            )
            }
        </li>
    )
}

export default ListItem