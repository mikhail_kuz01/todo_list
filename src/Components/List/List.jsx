import React from 'react'
import ListItem from './ListItem.jsx'

const List = ({ items, isRemovable, onRemove, onClick, onClickItem, activeItem, active }) => {
    return (
        <ul className='todo__list' onClick={onClick}>
            {items.map((item, index) => (
                <ListItem 
                    item={item}
                    key={index} 
                    icon={item.icon} 
                    title={item.name} 
                    color={item.color ? item.color.name : item.color}  
                    isRmv={isRemovable}
                    onRmv={onRemove}
                    onClickItem={onClickItem}
                    activeItem={activeItem}
                    active={active}
                />
            ))}
        </ul>
    )
}

export default List