import React, { useState, useEffect } from 'react'
import axios from 'axios'

import List from '../List/List.jsx'
import Circle from '../Circle/Circle.jsx'

import AddListSvg from '../../assets/img/add.svg' 
import CloseSvg from '../../assets/img/close.svg'

const AddButtonList = ({ onAdd, colors }) => {
    const [visiable, setVisiable] = useState(false)
    const [selectedColor, selectColor] = useState(1)
    const [isLoading, setIsLoading] = useState(false)
    const [inputValue, setInputValue] = useState('')

    useEffect(() => {
        if (Array.isArray(colors))
            selectColor(colors[0].id)
    }, [colors])

    const onClose = () => {
        setVisiable(false)
        selectColor(1)
        setInputValue('')
    }

    const addNewList = (e) => {
        e.preventDefault()

        if (!inputValue) {
            alert('Пожалуйста, введите название папки!')
            return
        }
        
        const list = {
            name: inputValue,
            colorId: selectedColor
        }

        setIsLoading(true)
        axios.post('http://localhost:3001/lists', list).then(({ data }) => {
            const color = colors.filter(color => color.id === selectedColor)[0].name
            const listObj = { ...data, color: { name: color }, tasks: [] }
            onAdd(listObj)
            onClose()
            
        }).finally(() => {
            setIsLoading(false)
        })
    }

    return (
        <div className='add-list'>
            <List 
                onClick={() => {setVisiable(true)}}
                items={[
                    {
                        icon: AddListSvg,
                        name: 'Добавить папку'
                    }
                ]}
            />
            {visiable && (
                <div className='add-list__popup'>
                    <img 
                        onClick={onClose}
                        src={CloseSvg} 
                        alt='Close button' 
                        className='add-list__popup-close-btn' 
                    />
                    <form onSubmit={addNewList}>
                        <input 
                            value={inputValue} 
                            onChange={(e) => setInputValue(e.target.value)}
                            type='text' 
                            placeholder='Название папки:'
                        />
                        <ul className='menu-circle'>
                            {colors.map((color, index) => (
                                <li key={index}>
                                    <Circle 
                                        onClick={() => selectColor(color.id)} 
                                        key={color.id} 
                                        color={color.name} 
                                        className={selectedColor === color.id && 'active'}
                                    />
                                </li>
                            ))}
                        </ul>
                        <button type='submit'>
                            {isLoading ? 'Добавление...' : 'Добавить'}
                        </button>
                    </form>
                </div>
            )}
        </div>
    )
}

export default AddButtonList 