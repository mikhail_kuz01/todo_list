import React, { useState, useEffect } from 'react'
import { Route, useHistory, useLocation } from 'react-router-dom'
import axios from 'axios'

import List from './List/List.jsx'
import AddButtonList from './AddButtonList/AddButtonList.jsx'
import Task from './Task/Task.jsx'

import listSvg from '../assets/img/list.svg'

const App = () => {
    const [lists, setLists] = useState(null)
    const [colors, setColors] = useState(null)
    const [activeItem, setActiveItem] = useState(null)
    let history = useHistory()
    let location = useLocation()

    useEffect(() => {
        axios.get('http://localhost:3001/lists?_expand=color&_embed=tasks').then(({ data }) => {
            setLists(data)
        })
        axios.get('http://localhost:3001/colors').then(({ data }) => {
            setColors(data)
        })
    }, [])
    
    useEffect(() => {
        const listId = location.pathname.split('lists/')[1]
        if (lists) {
            const list = lists.find(list => list.id === Number(listId))
            setActiveItem(list)
        }
    }, [lists, location.pathname])

    const onAddNewList = (obj) => {
        const newList = [ ...lists, obj ]
        setLists(newList)
    }

    const onEditListTitle = (id, title) => {
        const newList = lists.map(list => {
            if (list.id === id) { list.name = title }
            return list
        })   
        setLists(newList)
    }

    const addNewTask = (id, task) => {
        const newList = lists.map(list => {
            if (list.id === id) {
                list.tasks = [...list.tasks, task]
            }
            return list
        })
        setLists(newList)
    }

    const onSetCheck = (listId, id, data) => {
        const newList = lists.map(list => {
            if (list.id === listId) {
                list.tasks = list.tasks.map(task => {
                    if (task.id === id) { task = data }
                    return task
                }) 
            }
            return list
        })
        setLists(newList)
    }

    const onRemoveTask = (listId, taskId) => {
        const newList = lists.map(list => {
            if (list.id === listId) {
                list.tasks = list.tasks.filter(task => task.id !== taskId)
            }
            return list
        })
        setLists(newList)
    }

    const onEditTask = (listId, taskId, text) => {
        const newList = lists.map(list => {
            if (list.id === listId) {
                list.tasks.map(task => {
                    if (task.id === taskId) {
                        task.text = text
                    }
                    return task
                })
            }
            return list
        })
        setLists(newList)
    }

    return (
        <div className='todo'>
            <div className='todo__sidebar'>
                {lists && lists.length ? (
                    <List  
                    items={[
                        {
                            icon: listSvg,
                            name: 'Все задачи'
                        }
                    ]}
                    active={location.pathname === '/'}
                    onClickItem={() => {
                        history.push('/')
                    }}
                />
                ) : (null)
                }
                {lists ? (
                    <List 
                        items={lists} 
                        isRemovable 
                        onRemove={(id) => {
                            const newLists = lists.filter(item => item.id !== id)
                            setLists(newLists)
                        }}
                        onClickItem={list => {
                            history.push(`/lists/${list.id}`)
                        }}
                        activeItem={activeItem}
                    />
                    ) : ( 'Загрузка...' )
                }
                <AddButtonList onAdd={onAddNewList} colors={colors} />
            </div>
            <div className='todo__tasks'>
                <Route exact path='/'>
                    {lists &&
                        lists.map(list => (
                            <Task 
                                key={list.id}
                                list={list} 
                                onEditTitle={onEditListTitle} 
                                onAddTask={addNewTask} 
                                onSetCheck={onSetCheck}
                                onRemoveTask={onRemoveTask} 
                                onEditTask={onEditTask}
                                withoutEmpty
                            />
                        ))
                    }
                </Route>
                <Route path='/lists/:id'>
                    {lists && lists.length ? (
                        activeItem ? (
                            <Task 
                                list={activeItem} 
                                onEditTitle={onEditListTitle} 
                                onAddTask={addNewTask} 
                                onSetCheck={onSetCheck}
                                onRemoveTask={onRemoveTask} 
                                onEditTask={onEditTask}
                            />
                        ) : (
                            <h2 className='none-tasks'><span>Выберите папку</span></h2>
                        )
                    ) : (
                        <h2 className='none-tasks'><span>Папки отсутствуют</span></h2>
                    )}
                </Route>
            </div>
        </div>
    )
}

export default App