import React, { useState } from 'react'
import ReactDOM from 'react-dom'

const Modal = ({ isDelete, list, text, removeListItem, editTitle, removeTask, editTask }) => {
    const [confirm, setConfirm] = useState(false)
    const [inputValue, setInputValue] = useState(text)

    return ReactDOM.createPortal (
        <div id='modal-window'>
            <form 
                className='modal-window__form' 
                onSubmit={(e) => {
                    isDelete ? (
                        list ? (
                            removeListItem(confirm, e)
                        ) : (
                            removeTask(confirm, e)
                        )
                    ) : (
                        list ? (
                            editTitle(confirm, inputValue, e)
                        ) : (
                            editTask(confirm, inputValue, e)
                        )
                    )
                }} 
                onReset={(e) => {
                    isDelete ? (
                        list ? (
                            removeListItem(confirm, e)
                        ) : (
                            removeTask(confirm, e)
                        )
                    ) : (
                        list ? (
                            editTitle(confirm, inputValue, e)
                        ) : (
                            editTask(confirm, inputValue, e)
                        )
                    )
                }}
            >
                {isDelete ? (
                    <h2 className='modal-window__form_title'>Вы действительно хотите удалить {list ? 'папку' : 'задачу'}?</h2>
                ) : (
                    <>
                        <h2 className='modal-window__form_title'>Введите новый {list ? 'заголовок' : 'текст задачи'}:</h2>
                        <input type="text" placeholder={list ? 'Название папки:' : 'Название задачи:'} value={inputValue} onChange={(e) => setInputValue(e.target.value)} />
                    </>
                )}
                
                <div className='modal-window__form_btns'>
                    <button onClick={() => setConfirm(true)} type='submit'>Подтвердить</button>
                    <button onClick={() => setConfirm(false)} type='reset' className='reset-btn'>Отмена</button>
                </div>
            </form>
        </div>,
        document.querySelector('body')
    )
}

export default Modal