import React, { useState } from 'react'
import axios from 'axios'
import classNames from 'classnames'

import AddTaskForm from './AddTaskForm.jsx'
import Modal from '../Modal/Modal.jsx'

import editSvg from '../../assets/img/edit.svg'
import removeSvg from '../../assets/img/remove.svg'
import checkSvg from '../../assets/img/check.svg'

const Task = ({ list, onEditTitle, onAddTask, onSetCheck, onRemoveTask, onEditTask, withoutEmpty }) => {
    const [modalActive, setModalActive] = useState(false)
    const [deletion, setStatusDeletion] = useState(false)
    const [elem, setElem] = useState(null)
    const [isList, thisIsList] = useState(false)
    const [edTask, taskForEdit] = useState(null)

    const editTitle = (confirm, newTitle, e) => {
        e.preventDefault()

        if (confirm) {
            if (newTitle && newTitle !== list.name) {
                onEditTitle(list.id, newTitle)
                axios.patch('http://localhost:3001/lists/' + list.id, {
                    name: newTitle
                }).catch('Не удалось обновить название папки!')
            }
        }

        setModalActive(false)
        thisIsList(false)
    }

    const checkTask = (id, check) => {
        axios.patch('http://localhost:3001/tasks/' + id, {
            completed: !check
        }).then(({ data }) => {
            onSetCheck(list.id, id, data)
        }).catch('Не удалось поменять состояние выполнения задачи!')
    }

    const removeTask = (task, confirm, e) => {
        e.preventDefault();

        if (confirm) {
            axios.delete('http://localhost:3001/tasks/' + task.id).then(() => {
                onRemoveTask(list.id, task.id)
            }).catch('Не удалось удалить задачу!')
        }

        setModalActive(false)
        setStatusDeletion(false)
        taskForEdit(null)
    } 

    const editTask = (task, confirm, newText, e) => {
        e.preventDefault()

        if (confirm) {
            if (newText && newText !== task.text) {
                onEditTask(list.id, task.id, newText)
                axios.patch('http://localhost:3001/tasks/' + task.id, {
                    text: newText
                }).catch('Не удалось поменять текст задачи!')
            }
        }

        setModalActive(false)
        taskForEdit(null)
    }

    const getCoords = () => {
        setTimeout(() => {
            let box = document.querySelector('#modal-window')
            box.style.marginTop = `${elem.pageY - elem.clientY}px`
            box.style.transition = 'all 0.3s'
        }, 1)
    }

    const setOverflow = () => {
        if (document.body.style.overflow == 'auto' && modalActive) 
            document.body.style.overflow = 'hidden'
        else document.body.style.overflow = 'auto'  
    }

    return (
        <div className='tasks'>
            <div className="tasks__item">
                <h2 className={classNames('tasks__title', {[`circle--${list.color.name}`]: list.color})}>
                    <span>{list.name}
                        <img 
                            className='edit-btn' 
                            src={editSvg} 
                            alt='Edit icon' 
                            onClick={(e) => {
                                thisIsList(true)
                                setModalActive(true)
                                setElem(e)
                            }} 
                        />
                    </span>
                </h2>
                <ul className='tasks__menu'>
                    {list.tasks && !list.tasks.length && <h2 className='none-tasks'><span>Задачи отсутствуют</span></h2>}
                    {list.tasks && list.tasks.map(task => (
                        <li key={task.id} className='tasks__menu__item'>
                            <div className='tasks__menu__item-row' onClick={() => checkTask(task.id, task.completed)}>
                                <input id={`check${task.id}`} type='checkbox' defaultChecked={task.completed}/>
                                <label htmlFor={`check${task.id}`} className='checkbox'><img src={checkSvg} alt="Check icon" /></label>
                                <label htmlFor={`check${task.id}`} className='tasks__menu__item-text'>{task.text}</label>
                            </div>
                            <img 
                                className='edit-btn' 
                                src={editSvg} 
                                alt='Edit icon' 
                                onClick={(e) => {
                                    taskForEdit(task)
                                    setModalActive(true)
                                    setElem(e)
                                }} 
                            />
                            <img 
                                className='remove-btn' 
                                src={removeSvg} 
                                alt='Remove icon' 
                                onClick={(e) => {
                                    taskForEdit(task)
                                    setStatusDeletion(true)
                                    setModalActive(true)
                                    setElem(e)
                                }} 
                            />
                        </li>
                    ))}
                    {!withoutEmpty && 
                        <AddTaskForm 
                            list={list} 
                            onAddTask={onAddTask} 
                        />
                    }
                    {modalActive && (
                        setOverflow(),
                        getCoords(),
                        deletion ? (
                            <Modal 
                                isDelete
                                removeTask={(confirm, e) => removeTask(edTask, confirm, e)}
                            />
                        ) : ( 
                            isList ? (
                                <Modal 
                                    list
                                    text={list && list.name}
                                    editTitle={(confirm, newTitle, e) => editTitle(confirm, newTitle, e)}
                                />    
                            ) : (
                                <Modal 
                                    text={edTask && edTask.text}
                                    editTask={(confirm, newText, e) => editTask(edTask, confirm, newText, e)}
                                />    
                            )
                        )
                    )}
                    {!modalActive && setOverflow()}
                </ul>
            </div>
        </div>
    )
}

export default Task