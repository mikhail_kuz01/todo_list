import React, { useState } from 'react'
import axios from 'axios'

import addSvg from '../../assets/img/add.svg'

const AddTaskForm = ({ list, onAddTask }) => {

    const [visiable, setVisiable] = useState(true)
    const [isLoading, setIsLoading] = useState(false)
    const [inputValue, setInputValue] = useState('')

    const onClose = () => {
        setVisiable(true)
        setInputValue('')
    }

    const createTasks = (e) => {
        e.preventDefault()

        if (!inputValue) {
            alert('Пожалуйста, введите название папки!')
            return
        }

        const task = {
            listId: list.id,
            text: inputValue,
            completed: false
        }

        setIsLoading(true)
        axios.post('http://localhost:3001/tasks', task).then(({ data }) => {
            onAddTask(list.id, data)
            onClose()
        }).finally(() => {
            setIsLoading(false)
        })
    }

    return (
        <div className='tasks__form'>
            {visiable ? (
                <div className='tasks__form-new' onClick={() => setVisiable(false)}>
                    <img src={addSvg} alt="Add icon" />
                    <span>Новая задача</span>
                </div>
            ) : (
                <form className='tasks__form-open' onSubmit={createTasks} onReset={onClose}>
                    <input 
                        value={inputValue} 
                        onChange={(e) => setInputValue(e.target.value)}
                        type='text' 
                        placeholder='Название задачи: ' 
                    />
                    <div className='tasks__form-open-btns'>
                        <button type='submit'>{!isLoading ? 'Добавить задачу' : 'Добавление...'}</button>
                        <button type='reset' className='reset-btn'>Отмена</button>
                    </div>
                </form>
            )}
        </div>
    )
}

export default AddTaskForm