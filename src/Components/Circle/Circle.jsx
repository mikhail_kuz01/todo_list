import React from 'react'
import classNames from 'classnames'

const Circle = ({ color, onClick, className }) => {
    return (
        <span 
            className={classNames('circle', {[`circle--${color}`]: color}, className)} 
            onClick={onClick}>
        </span>
    )
}

export default Circle